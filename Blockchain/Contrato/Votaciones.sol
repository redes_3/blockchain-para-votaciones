pragma solidity ^0.5.0;

contract Votacion {
   uint candidatoA;
   uint candidatoB;
   uint candidatoC;
   
   uint votosCandidatoA;
   uint votosCandidatoB;
   uint votosCandidatoC;
   
   string descripcion;
   event Voto(string nombreElecciones, address _votante, uint _candidatoVotado);
   
   constructor () public{
       candidatoA = 1;
       candidatoB = 2;
       candidatoC = 3;
       
       votosCandidatoA = 0;
       votosCandidatoB = 0;
       votosCandidatoC = 0;
       
       descripcion = "Votaciones regionales 2020";
   }
   //Se realiza el voto, todos los usuario tendran tan solo 1.3 ETH por lo que solo podran votar una vez
   function votar(uint _candidato) public payable{
       //Siempre y cuando se envie un ether
       if (msg.value >= 1000000000000000000) {  
            if(_candidato == candidatoA){
                votosCandidatoA+=1;
                //Se emite un evento que sera accesible para siempre en la blockchain
                emit Voto(descripcion,msg.sender, _candidato);
            }else if(_candidato ==  candidatoB){
                votosCandidatoB+=1;
                //Se emite un evento que sera accesible para siempre en la blockchain
                emit Voto(descripcion,msg.sender, _candidato);
            }else if(_candidato == candidatoC){
                votosCandidatoC+=1;
                //Se emite un evento que sera accesible para siempre en la blockchain
                emit Voto(descripcion,msg.sender, _candidato);
            }
       }
        
       
   }
   //Se obtienen la cantidad de votos que tiene un candidato
   function getVotosCandidato(uint _candidato) public view returns (uint votos){
        if(_candidato == candidatoA){
            return votosCandidatoA;
        }else if(_candidato ==  candidatoB){
            return votosCandidatoB;
        }else if(_candidato == candidatoC){
            return votosCandidatoC;
        }
   }
}
