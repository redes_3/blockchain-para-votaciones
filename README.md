# Implementacion de una blockchain para votaciones
Se realiza la implementacion de una blockchain privada con Geth, posteriormente se establece el enlace de un nodo con un servidor por medio del framework Web3py en python. Finalmente se monta una interfaz web (con flask) que se comunica con el framework antes mencionado el cual finalmente se comunica con la blockchain y el contrato inteligente de las votaciones.

En este repositorio se encuentra la implementacion de todo el proyecto a excepcion de los nodos, sin embargo en Blockchain/ se encuentra el fichero genesis redes.json que permite recrear la estructura de la blockchain, adicionalmente se incluye la informacion tal cual de un nodo a modo de poder visualizar su estructura.
## Universidad distrital Francisco Jose de Caldas
### Santiago Rincon Robelto 20172020084