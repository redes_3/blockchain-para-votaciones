import time
from web3 import Web3, HTTPProvider
import contract


class Contrato:
    w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:8501"))
    wallet_private_key = ""
    wallet_address = ""
    contract_address = "0x7529c60BBa7C805DE0fEa71792Da77555A92c22d"
    contract = w3.eth.contract(address=contract_address, abi=contract.abi)

    def __init__(self, pub, priv):
        self.wallet_private_key = priv
        self.wallet_address = pub

    def votar(self, candidato):
        try:
            # Cantidad de transacciones de la direccion
            nonce = self.w3.eth.getTransactionCount(self.wallet_address)
            # Hacemos la transaccion para realizar el voto
            txn_dict = self.contract.functions.votar(candidato).buildTransaction(
                {
                    "chainId": 12345,
                    "gas": 140000,
                    "gasPrice": self.w3.toWei("40", "gwei"),
                    "nonce": nonce,
                    "value": self.w3.toWei(1, "ether"),
                }
            )
            # Se firma la transaccion
            signed_txn = self.w3.eth.account.signTransaction(
                txn_dict, private_key=self.wallet_private_key
            )
            # Se envia a la red
            result = self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)

            # Se espera a recibir el comprobante
            try:
                tx_receipt = self.w3.eth.getTransactionReceipt(result)
            except:
                tx_receipt = None
            count = 0
            while tx_receipt is None and (count < 30):

                time.sleep(10)

                tx_receipt = self.w3.eth.getTransactionReceipt(result)

            if tx_receipt is None:
                return {"status": "failed", "error": "timeout"}
            # Se rerna el hash de la transaccion
            return str(result.hex())
        except:
            return "NO SE REALIZO EL VOTO"

    # Obtencion de la informacion de algun voto
    def getNombreElecciones(self, hash_voto):
        result = self.contract.events.Voto().processReceipt(
            self.w3.eth.getTransactionReceipt(hash_voto)
        )
        return result[0]["args"]["nombreElecciones"]

    def getVotante(self, hash_voto):
        result = self.contract.events.Voto().processReceipt(
            self.w3.eth.getTransactionReceipt(hash_voto)
        )
        return result[0]["args"]["_votante"]

    def getCandidatoVotado(self, hash_voto):
        result = self.contract.events.Voto().processReceipt(
            self.w3.eth.getTransactionReceipt(hash_voto)
        )
        return result[0]["args"]["_candidatoVotado"]

    def getResultadoFinalCandidatos(self):
        salida = []
        salida.append(
            {
                "candidato": 1,
                "votos": self.contract.functions.getVotosCandidato(1).call(),
            }
        )
        salida.append(
            {
                "candidato": 2,
                "votos": self.contract.functions.getVotosCandidato(2).call(),
            }
        )
        salida.append(
            {
                "candidato": 3,
                "votos": self.contract.functions.getVotosCandidato(3).call(),
            }
        )

        return salida

    def varios():
        # prstring(send_ether_to_contract(0.03))
        # prstring(estaAprobada("0x3D6fe25264EA08469776D4930a95e7De21a7F033"))
        # prstring(broadcast_an_opinion("siiii"))
        """
        getTx("0x049a9eadb841aa6acbc3840e69659d3a7a45611736121618111c7f37367c91f4")
        getTx("0xf553da9be2acbfbcfeea046af0cb4fc4940ab306c87a30e1621564a35167d8cb")
        """
        prstring(getTx(sendDeclararID(1113, 0.001)))


if __name__ == "__main__":

    contrato = Contrato("", "",)
    # print(contrato.votar(2))
    # print(contrato.getResultadoFinalCandidatos())
    print(
        contrato.getInfoVoto(
            "0x7925033b8ae34c78470827e0205f3fbc22c2c84a460f0e0990eed6cb5b739a83"
        )
    )
