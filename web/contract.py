abi = [
    {
        "anonymous": False,
        "inputs": [
            {
                "indexed": False,
                "internalType": "string",
                "name": "nombreElecciones",
                "type": "string",
            },
            {
                "indexed": False,
                "internalType": "address",
                "name": "_votante",
                "type": "address",
            },
            {
                "indexed": False,
                "internalType": "uint256",
                "name": "_candidatoVotado",
                "type": "uint256",
            },
        ],
        "name": "Voto",
        "type": "event",
    },
    {
        "constant": False,
        "inputs": [
            {"internalType": "uint256", "name": "_candidato", "type": "uint256"}
        ],
        "name": "votar",
        "outputs": [],
        "payable": True,
        "stateMutability": "payable",
        "type": "function",
    },
    {
        "inputs": [],
        "payable": False,
        "stateMutability": "nonpayable",
        "type": "constructor",
    },
    {
        "constant": True,
        "inputs": [
            {"internalType": "uint256", "name": "_candidato", "type": "uint256"}
        ],
        "name": "getVotosCandidato",
        "outputs": [{"internalType": "uint256", "name": "votos", "type": "uint256"}],
        "payable": False,
        "stateMutability": "view",
        "type": "function",
    },
]
