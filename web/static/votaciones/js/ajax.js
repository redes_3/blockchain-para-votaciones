$(function() {
  $("#btnResultados").click(function(){
    location.href = '/resultados';
  })
    $( "#btnVotar" ).click(function() {
      console.log("DATOS ENVIADOS")
      console.log("Publica: "+$("#dirPublica").val())
      console.log("Privada: "+$("#dirPrivada").val())
      console.log("Candidato: "+$("#slcCandidatos :selected").val())

    $.ajax({
      url: '/votar',
      type: 'GET',
      data: {
            publica: $("#dirPublica").val(),
            privada:$("#dirPrivada").val(),
            candidato:$("#slcCandidatos :selected").val(),
            },
      success: function(data) {
      console.log(data)
      if(data.Estado_voto == "realizado"){
        $("#spanComprobante").text("Voto realizado con exito!, comprobante: "+data.Comprobante)
        $("#spanComprobante").addClass("border")
      }else{
        $("#spanComprobante").text("El voto no se realizo")
        $("#spanComprobante").addClass("border")
      }

      //Dependiendo de lo que salga aqui toca redireccionar o no

      },
      error: function(e) {
    	//called when there is an error
    	//console.log(e.message);
      }
    });
  });
});
