from flask import Flask
from flask import render_template, request
from flask import jsonify
from flask import Flask, session

from Contrato import Contrato

app = Flask(__name__, static_folder="static")


@app.route("/", methods=["GET"])
def catalogo():
    return render_template("votaciones.html")


@app.route("/votar", methods=["GET"])
def votar(pub=None, pri=None):
    try:
        publica = request.args.get("publica")
        privada = request.args.get("privada")
        candidato = request.args.get("candidato")

        contrato = Contrato(publica, privada,)
        hash_voto = contrato.votar(int(candidato))
        return jsonify(
            {
                "Estado_voto": "realizado",
                "Comprobante": hash_voto,
                "Nombre elecciones": contrato.getNombreElecciones(hash_voto),
                "Votante": contrato.getVotante(hash_voto),
                "Candidato votado": contrato.getCandidatoVotado(hash_voto),
            }
        )
    except:
        return jsonify(
            {"Estado_voto": "NO realizado", "Comprobante": "Voto NO efectuado",}
        )


@app.route("/resultados", methods=["GET"])
def resultados():

    contrato = Contrato("", "",)
    return jsonify(contrato.getResultadoFinalCandidatos())


app.run(debug=True)
